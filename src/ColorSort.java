public class ColorSort {

   enum Color {red, green, blue}

   public static void main(String[] param) {
   }

   public static void reorder(Color[] balls) {
      int blue = 0;
      int green = 0;
      int red = 0;

      for (Color elem: balls) {
         if (elem == Color.red) {
            red++;
         }
         if (elem == Color.green) {
            green++;
         }
         if (elem == Color.blue) {
            blue++;
         }
      }

      for (int i = 0; i<red; i++) {
         balls[i] = Color.red;
      }

      for (int i = red; i<red+green; i++) {
         balls[i] = Color.green;
      }

      for (int i = red+green; i<red+green+blue; i++) {
         balls[i] = Color.blue;
      }
   }
}

